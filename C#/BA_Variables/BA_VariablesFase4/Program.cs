﻿using System;
using System.Text;

namespace BA_VariablesFase4
{
    class Program
    {
        public const int leapRepeat = 4;

        static void Main(string[] args)
        {
            Console.WriteLine("Hola! Escribe tu nombre por favor!");
            string name = Console.ReadLine();
            Console.WriteLine("Escribe tu primer apellido por favor!");
            string lastname1 = Console.ReadLine();
            Console.WriteLine("Escribe tu segundo apellido por favor!");
            string lastname2 = Console.ReadLine();

            Console.WriteLine("Hola! Escribe tu día de nacimiento!");
            string day = Console.ReadLine();
            Console.WriteLine("Escribe tu mes de nacimiento!");
            string month = Console.ReadLine();
            Console.WriteLine("Escribe tu año de nacimiento");
            string birthYearStr = Console.ReadLine();
            string bar = "/";

            string student = name + " " + lastname1 + " " + lastname2;
            string date = String.Concat(day, bar, month, bar, birthYearStr);

            int birthYear;
            int leapYear = 1948;
            bool isLeapYear = false;

            StringBuilder sb = new StringBuilder();
            sb.AppendLine("El meu nom es " + student);
            sb.AppendLine("Vaig néixer el " + date);

            if (Int32.TryParse(birthYearStr, out birthYear))
            {

                while (birthYear >= leapYear)
                {
                    if (leapYear == birthYear)
                    {
                        isLeapYear = true;
                        break;
                    }
                    else
                        leapYear += leapRepeat;
                }

                Console.WriteLine();
                if (isLeapYear == true)
                {
                    sb.AppendLine("El meu any de naixement és de traspàs.");
                    Console.WriteLine(sb.ToString());
                }
                    
                else
                {
                    sb.AppendLine("El meu any de naixement no és de traspàs.");
                    Console.WriteLine(sb.ToString());
                }
                    
            }
            else
                Console.WriteLine("Introduce un número válido!");

        }
    }
}
